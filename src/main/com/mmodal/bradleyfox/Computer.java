package com.mmodal.bradleyfox;

import java.util.ArrayList;

/**
 * Created by bfox on 3/2/16.
 */
public class Computer {

    private ArrayList<Double> arrayDouble;
    private int windowSize;

    public Computer( int windowSize, ArrayList<Double> arrayDouble) {
        this.arrayDouble = arrayDouble;
        this.windowSize = windowSize;
    }

    public ArrayList<Double> computeWindowAverage() {
        double runningTotal = 0;
        ArrayList<Double> averageArray = new ArrayList<>();

        for (int index = 0; index < arrayDouble.size(); index++) {
            if (index >= windowSize) {
                runningTotal -= arrayDouble.get(index - windowSize);
            }
            runningTotal += arrayDouble.get(index);
            if (runningTotal == 0.0) {
                averageArray.add(0.0);
            } else {
                if (index < windowSize) {
                    averageArray.add(runningTotal / (index + 1));
                } else {
                    averageArray.add(runningTotal / windowSize);
                }
            }
        }
        return averageArray;
    }


}
