package com.mmodal.bradleyfox;


import java.io.Console;
import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by bfox on 2/25/16.
 */
public class windowAverageCLI {

    public static void main (String[] args) {
        Console console = System.console();
        if (console == null) {
            System.err.println("Failure: No console");
            System.exit(1);
        }

        int windowSize = getWindowSize(console, true);
        ArrayList<Double> array = getArray(console, true);

        Computer computer = new Computer(windowSize, array);
        ArrayList<Double> averageArray = computer.computeWindowAverage();

        System.out.print("Results: " + averageArray + "\n");
    }

    private static int getWindowSize(Console console, Boolean retry) {
        String windowSize = console.readLine("Enter the window size(>0): ");
        try {
            int parsedWindowSize = Integer.parseInt(windowSize);
            if(parsedWindowSize > 0) {
                return parsedWindowSize;
            }
            System.out.println("Enter a value greater than 0. Please try again.");
            return getWindowSize(console, false);
        } catch (NumberFormatException e) {
            if(retry) {
                System.out.println("Could not parse. Please try again.");
            } else {
                System.err.println("Could not parse. Exiting.");
                System.exit(0);
            }
            return getWindowSize(console, false);
        }
    }

    private static ArrayList<Double> getArray(Console console, Boolean retry) {
        ArrayList<Double> parsedArray = new ArrayList<>();
        String array = console.readLine("Enter the array(1,2,3,.5): ");
        try {
            String[] parsedStringArray = array.split(",\\s?");
            for(String number : parsedStringArray) {
                parsedArray.add(Double.parseDouble(number));
            }
            return parsedArray;
        } catch (NumberFormatException e) {
            if(retry) {
                System.out.println("Could not parse. Please try again.");
            } else {
                System.err.println("Could not parse. Exiting.");
                System.exit(0);
            }
            return getArray(console, false);
        }
    }
}
