package com.mmodal.bradleyfox;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by bfox on 3/2/16.
 */
public class ComputerTest {

    @Test
    public void testComputeWindowAverage() {
        int windowSize = 5;
        ArrayList<Double> inputArray = new ArrayList<>();
        inputArray.add(0.0);
        inputArray.add(1.0);
        inputArray.add(2.0);
        inputArray.add(3.0);

        Computer computer = new Computer(windowSize, inputArray);
        ArrayList<Double> testArray = computer.computeWindowAverage();

        assertEquals(testArray.get(0), 0.0, 2);
        assertEquals(testArray.get(1), 0.5, 2);
        assertEquals(testArray.get(2), 1.0, 2);
        assertEquals(testArray.get(3), 2.0, 2);
    }
}
