# windowAverage #

WindowAverage takes a window size and an array of doubles. It computes the average within that window size.

### Build/Run ###

`gradle jar`

`java -jar windowAverage-1.0-SNAPSHOT.jar`

When run, it will prompt you for the window size and the array.

Window size accepts any integer greater than zero.

Array accepts a list delimited by commas. For example: 0,1,2,3